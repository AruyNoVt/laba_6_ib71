
class Caesar():
    _cipher_mode = True

    def setMode(self, mode):
        self._cipher_mode = mode

    def needKey(self):
        return False

    def cipher(self, text):
        if self._cipher_mode == True: return self.encrypt(text)
        else: return self.decrypt(text)

    def encrypt(self, text):
        ciphertext = ""
        n = 0
        for i in text:
            ciphertext += shiftAlpha(i, 3)
            n += 1
        return ciphertext

    def decrypt(self, ciphertext):
        text = ""
        n = 0
        for i in ciphertext:
            text += shiftAlpha(i, -3)
        return text

class Vigener():
    _cipher_mode = True
    _keyword = ''

    def setKey(self, keyword):
        self._keyword = keyword

    def setMode(self, mode):
        self._cipher_mode = mode

    def needKey(self):
        return True

    def cipher(self, text):
        if self._cipher_mode == True: return self.encrypt(text)
        else: return self.decrypt(text)

    def encrypt(self, text):
        ciphertext = ""
        n = 0
        for i in text:
            ciphertext += shiftAlpha(i, indexAlpha(self._keyword[n % len(self._keyword)]))
            n += 1
        return ciphertext

    def decrypt(self, ciphertext):
        text = ""
        n = 0
        for i in ciphertext:
            text += shiftAlpha(i, -indexAlpha(self._keyword[n % len(self._keyword)]))
        return text

def indexAlpha(letter):
    let = ord(letter)
    if (let >= ord('A') and let <= ord('Z')): let -= ord('A')
    elif (let >= ord('А') and let <= ord('Я')): let -= ord('А')
    elif (let >= ord('a') and let <= ord('z')): let -= ord('a')
    elif (let >= ord('а') and let <= ord('я')): let -= ord('а')
    else: let = -1
    return let+1

def shiftAlpha(letter, count):
    let = ord(letter)
    if (let >= ord('A') and let <= ord('Z')): let = shiftCycle(ord('A'), ord('Z'), let, count)
    elif (let >= ord('А') and let <= ord('Я')): let = shiftCycle(ord('А'), ord('Я'), let, count)
    elif (let >= ord('a') and let <= ord('z')): let = shiftCycle(ord('a'), ord('z'), let, count)
    elif (let >= ord('а') and let <= ord('я')): let = shiftCycle(ord('а'), ord('я'), let, count)
    letter = chr(let)
    return letter

def shiftCycle(a, b, N, count):
    N += count
    if N > b: N -= b-a+1
    elif N < a: N += b-a+1
    return N