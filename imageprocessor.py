from PIL import Image, ImageFilter

class ImageProcessor():
    def __init__(self, images = []):
        self.images = images

    def processImage(self):
        for i in self.images:
            img = Image.open(i)
            img = img.filter(ImageFilter.BoxBlur(15))
            img.save(i)
            img.close()
        return self.images