import qrcode

class QREncoder():
    def encode(text, savePath='qrCode.jpg'):
        qrImg = qrcode.make(text)
        qrImg.save(savePath)
        return savePath