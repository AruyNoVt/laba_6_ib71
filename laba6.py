# -*- coding: utf-8 -*-

import vk_api
import cryptograph as c, qrencoder as q, imageprocessor as p
import time, requests
from enum import Enum
from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.keyboard import VkKeyboard, VkKeyboardColor

vk_session = vk_api.VkApi(token='1de45b5216b95c434e51b4d22474412087c196d6acf2553b449c81acb47574f4ba7cec66a151d0034f41b')
vk = vk_session.get_api()

class States(Enum):
    MAIN = 0
    TYPE = 1
    CIPH_TYPE = 2
    CIPH_KEY = 3
    CIPH_TEXT = 4
    QRCODE = 5
    BLURIMG = 6

class user:
    user_id = None
    step = States.MAIN
    def __init__(self, user_id):
        self.user_id = user_id
    def getId(self):
        return self.user_id
    def getStep(self):
        return self.step
    def setStep(self, step):
        self.step = step
    def send_message(self, message='', attachment=[], keyboard=None):
        if attachment != []:
            upload = vk_api.VkUpload(vk_session)
            uploaded_photos = upload.photo_messages(attachment)
            attachment = []
            for photo in uploaded_photos:
                attachment.append('photo{}_{}'.format(photo['owner_id'], photo['id']))
        vk.messages.send(
            user_id=self.user_id,
            message=message,
            attachment=','.join(attachment),
            keyboard=keyboard.get_keyboard() if keyboard is not None else None,
            random_id = (time.time() * 10 ** 5)
            )

def get_img_size(sizes, size_type):
    for size in sizes:
        if size['type'] == size_type:
            return size

def parse_json_format(attachments):
    urls = dict()
    for attach in attachments:
        photo_id = attach['photo']['id']
        size = None
        for size_type in ['w', 'y', 'z', 'x', 'm', 's']:
            size = get_img_size(attach['photo']['sizes'], size_type)
            if size is not None:
                break
        url = size['url']
        urls[photo_id] = url
    return urls

def download(fileLink):
    fname = str(time.time()) + 'savedFile.jpg'
    with open(fname, 'wb') as handle:
        response = requests.get(fileLink, stream=True)
        if not response.ok:
            print(response)
        for block in response.iter_content(1024):
            if not block:
                break
            handle.write(block)
    return fname

def main():
    longpoll = VkLongPoll(vk_session)
    userList = dict()
	
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                if not event.user_id in userList.keys():
                    userList[event.user_id] = user(event.user_id)
                userAcc = userList[event.user_id]

                if userAcc.getStep() == States.MAIN:
                    userAcc.setStep(States.TYPE)

                    keyboard = VkKeyboard(one_time=True)
                    keyboard.add_button('Шифр Цезаря', color=VkKeyboardColor.DEFAULT)
                    keyboard.add_button('Шифр Виженера', color=VkKeyboardColor.DEFAULT)
                    keyboard.add_line()
                    keyboard.add_button('Текст в QR-код', color=VkKeyboardColor.DEFAULT)
                    keyboard.add_button('Размытие изображений', color=VkKeyboardColor.DEFAULT)

                    userAcc.send_message('Добро пожаловать, вас приветствует бот.\nВыберите действие, которое хотите выполнить, при помощи кнопок.', keyboard=keyboard)

                elif userAcc.getStep() == States.TYPE:
                    if event.text == 'Шифр Цезаря':
                        userAcc.cryptor = c.Caesar()
                        userAcc.setStep(States.CIPH_TYPE)

                        keyboard = VkKeyboard(one_time=True)
                        keyboard.add_button('Шифрование', color=VkKeyboardColor.POSITIVE)
                        keyboard.add_button('Дешифрование', color=VkKeyboardColor.NEGATIVE)

                        userAcc.send_message(
                            'Вы выбрали Шифр Цезаря. Какое действие с текстом Вы хотите сделать?',
                            keyboard=keyboard)
                    elif event.text == 'Шифр Виженера':
                        userAcc.cryptor = c.Vigener()
                        userAcc.setStep(States.CIPH_TYPE)

                        keyboard = VkKeyboard(one_time=True)
                        keyboard.add_button('Шифрование', color=VkKeyboardColor.POSITIVE)
                        keyboard.add_button('Дешифрование', color=VkKeyboardColor.NEGATIVE)

                        userAcc.send_message(
                            'Вы выбрали Шифр Вижинера. Какое действие с текстом Вы хотите сделать?',
                            keyboard=keyboard)
                    elif event.text == 'Текст в QR-код':
                        userAcc.setStep(States.QRCODE)
                        userAcc.send_message('Введите текст, который нужно преобразовать в QR-код:')
                    elif event.text == 'Размытие изображений':
                        userAcc.setStep(States.BLURIMG)
                        userAcc.send_message('Загрузите и отправьте фотографии, которые необходимо преобразовать:')
                    else:
                        keyboard = VkKeyboard(one_time=True)
                        keyboard.add_button('Шифр Цезаря', color=VkKeyboardColor.DEFAULT)
                        keyboard.add_button('Шифр Виженера', color=VkKeyboardColor.DEFAULT)
                        keyboard.add_line()
                        keyboard.add_button('Текст в QR-код', color=VkKeyboardColor.DEFAULT)
                        keyboard.add_button('Размытие изображений', color=VkKeyboardColor.DEFAULT)
                        userAcc.send_message('Такой команды не существует. Попробуйте ещё раз.', keyboard=keyboard)

                elif userAcc.getStep() == States.CIPH_TYPE:
                    if event.text == 'Шифрование' or event.text == 'Дешифрование':
                        if event.text == 'Шифрование':
                            userAcc.cryptor.setMode(True)
                            userAcc.send_message('Вы выбрали режим шифрования.')
                        else:
                            userAcc.cryptor.setMode(False)
                            userAcc.send_message('Вы выбрали режим дешифрования.')

                        if userAcc.cryptor.needKey():
                            userAcc.setStep(States.CIPH_KEY)
                            userAcc.send_message('Введите ключ, который будет использоваться для преобразования текста:')
                        else:
                            userAcc.setStep(States.CIPH_TEXT)
                            userAcc.send_message('Введите текст, который нужно преобразовать:')
                    else:
                        keyboard = VkKeyboard(one_time=True)
                        keyboard.add_button('Шифрование', color=VkKeyboardColor.POSITIVE)
                        keyboard.add_button('Дешифрование', color=VkKeyboardColor.NEGATIVE)
                        userAcc.send_message('Такой команды не существует. Попробуйте ещё раз.', keyboard=keyboard)


                elif userAcc.getStep() == States.CIPH_KEY:
                    if len(event.text) > 0:
                        userAcc.setStep(States.CIPH_TEXT)
                        userAcc.cryptor.setKey(event.text)
                        userAcc.send_message('Вы ввели ключ. Теперь введите текст, который нужно преобразовать:')
                    else:
                        userAcc.send_message('Произошла ошибка. Повторите попытку ввода ключа:')

                elif userAcc.getStep() == States.CIPH_TEXT:
                    if len(event.text) > 0:
                        userAcc.setStep(States.MAIN)
                        result = userAcc.cryptor.cipher(event.text)

                        keyboard = VkKeyboard(one_time=True)
                        keyboard.add_button('В начало', color=VkKeyboardColor.PRIMARY)

                        userAcc.send_message('Текст успешно преобразован. Результат:\n' + str(result), keyboard=keyboard)
                    else:
                        userAcc.send_message('Произошла ошибка. Повторите попытку ввода текста:')

                elif userAcc.getStep() == States.QRCODE:
                    if len(event.text) > 0:
                        userAcc.setStep(States.MAIN)
                        img = q.QREncoder.encode(event.text)

                        keyboard = VkKeyboard(one_time=True)
                        keyboard.add_button('В начало', color=VkKeyboardColor.PRIMARY)

                        userAcc.send_message('Текст успешно преобразован в QR-код. Результат:,', attachment=[ img ],
                                          keyboard=keyboard)
                    else:
                        userAcc.send_message('Произошла ошибка. Повторите попытку ввода текста:')

                elif userAcc.getStep() == States.BLURIMG:
                    if event.attachments != {}:
                        photo_ids = []
                        k = list(event.attachments.keys())
                        for item_type, item_value in event.attachments.items():
                            if item_value == "photo":
                                photo_ids.append(event.attachments[k[k.index(item_type) + 1]])
                        if len(photo_ids) > 0:
                            jsonoutput = vk_session.method('messages.getHistory', {'user_id':event.peer_id,'count':1})['items'][0]['attachments']
                            urls_lst = parse_json_format(jsonoutput)

                            photo_lst = []
                            for photo_id in photo_ids:
                                pid = int(photo_id.split(str(event.user_id) + "_")[1])
                                if pid in urls_lst:
                                    photo_lst.append( download(urls_lst[pid]) )

                            imgProcessor = p.ImageProcessor(photo_lst)

                            keyboard = VkKeyboard(one_time=True)
                            keyboard.add_button('В начало', color=VkKeyboardColor.PRIMARY)

                            userAcc.send_message('Фотографии успешно преобразованы:', attachment=imgProcessor.processImage(), keyboard=keyboard)
                            userAcc.setStep(States.MAIN)
                        else:
                            userAcc.send_message('Произошла ошибка. Прикрепите фотографии к Вашему сообщению.')
                    else:
                        userAcc.send_message('Произошла ошибка. Прикрепите фотографии к Вашему сообщению.')

if __name__ == '__main__':
    main()
